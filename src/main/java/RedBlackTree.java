import java.util.Scanner;

public class RedBlackTree {

    public static void main(String[] args) {
        new RedBlackTree().createRBT();
    }

    //create the node class
    private class Node {
        int key;
        String value;

        boolean isRed = true;
        boolean isLeft = false;

        Node left;
        Node right;
        Node parent;
    }

    Scanner scan = new Scanner(System.in);

    //declare the instance variable for the root of the tree
    Node root = null;

    //parameters:   tree root and user input
    //return:   creates a Red Black Tree
    private void createRBT() {

        //take user input and insert in the tree until the user requires to stop
        while (true) {
            //create a break point
            System.out.println("\n1. Insert a new node\n2. Remove an existing node\n3. Search for a node\n4. Show Red Black Tree\n5. Delete Red Black Tree\n6. Exit the program\nWhat do you want to do next?");
            byte userChoice = scan.nextByte();
            switch (userChoice) {
                case 1:
                    case1();
                    break;
                case 2:
                    case2();
                    break;
                case 3:
                    case3();
                    break;
                case 4:
                    showRBT(root);
                    break;
                case 5:
                    deleteRBT();
                    System.out.println("Tree deleted!");
                    break;
                case 6:
                    System.out.println("All the tree info will be lost!");
                    return;
                default:
                    System.out.println("The only available choices are from 1 to 5");
                    break;
            }

        }

    }

    //parameter:    the root and the key the users wants to find
    //return:   the first node which has the sought key or null
    private Node searchNode(Node node, int key) {

        Node foundNode = null;

        //in case the tree is empty
        if (node == null) {
            return null;
        }

        //if the current node has the key the user is searching
        if (node.key == key) {
            return node;
        }

        //if there is a left child
        if (node.left != null) {
            foundNode = searchNode(node.left, key);

            //break the loop
            if (foundNode != null) {
                return foundNode;
            }
        }

        //if there is a right child
        if (node.right != null) {
            foundNode = searchNode(node.right, key);

            //break the loop
            if (foundNode != null) {
                return foundNode;
            }
        }

        //if the sought node is not the current node and in none of the branches
        return foundNode;
    }

    //parameters:   the tree root
    //return:   displays the RBT in the terminal
    private void showRBT(Node node) {

        if (node == null) {
            System.out.println("The tree is empty");

        } else {

            if (node.parent == null) {
                System.out.print("Node.key = " + node.key + "; Node.isRed = " + node.isRed + "; Node.parent = " + node.parent + "; Node.isLeft = " + node.isLeft);
            } else {
                System.out.print("Node.key = " + node.key + "; Node.isRed = " + node.isRed + "; Node.parent = " + node.parent.key + "; Node.isLeft = " + node.isLeft);
            }

            if (node.left != null) {
                System.out.print("; Node.left = " + node.left.key);
            }

            if (node.right != null) {
                System.out.print("; Node.right = " + node.right.key);
            }
            System.out.println();

            if (node.left != null) {
                showRBT(node.left);
            }

            if (node.right != null) {
                showRBT(node.right);
            }
        }

    }

    //parameters:   sub-tree root and the node to insert
    //return:   inserts the new node in the Red Black Tree
    private void insertNode(Node parent, Node newNode) {
        if (parent == null) {
            //if the tree is empty

            newNode.isRed = false;
            root = newNode;

        } else if (newNode.key < parent.key) {
            //if the new node is smaller than the parent node

            if (parent.left == null) {
                //if the parent does not have a left child

                newNode.parent = parent;
                newNode.isLeft = true;
                parent.left = newNode;

            } else {
                //if the parent does have a left child

                insertNode(parent.left, newNode);
            }
        } else {
            //if the new node is larger than the parent node

            if (parent.right == null) {
                //if the parent does not have a right child

                newNode.parent = parent;
                parent.right = newNode;

            } else {
                //if the parent does have a right child

                insertNode(parent.right, newNode);
            }
        }
    }

    //parameters:   key of a node which is to be deleted
    //return:   deletes the node and adjusts the tree
    private void removeNode(Node toBeRemoved) {

        //Step1
        Node replacementNode;
        {
            if (toBeRemoved.left == null && toBeRemoved.right == null) {
                replacementNode = null;
            } else if (toBeRemoved.right == null && toBeRemoved.left != null) {
                replacementNode = toBeRemoved.left;
            } else {
                replacementNode = toBeRemoved.right;
            }
        }

        //Step2
        {
            if (!toBeRemoved.isRed && (replacementNode == null || !replacementNode.isRed)) {
                //if the deleted node is black and the replacement node is black or null

                //proceed to cases

            } else if (toBeRemoved.isRed && !replacementNode.isRed) {
                //if the deleted node is red and the replacement node is black

                //replacement node changes to red
                replacementNode.isRed = true;

                //proceed to cases

            } else if (!toBeRemoved.isRed && replacementNode.isRed) {
                //if the deleted node is black and the replacement node is red

                //replacement node changes to black
                replacementNode.isRed = false;

                //do the replacement
                replaceNodes(toBeRemoved, replacementNode);

                //we're done

            } else if (toBeRemoved.isRed && replacementNode.isRed) {
                //if the deleted node is red and the replacement node is red

                //do the replacement
                replaceNodes(toBeRemoved, replacementNode);

                //we're done
            }

        }
    }

    //parameters:   the node which might cause a violation
    //return:   calls the function which corrects the tree
    private void checkRBT(Node node) {
        if (node.parent == null) {
            //if the current node is the root we make sure it is black;

            if (node.isRed) {
                node.isRed = false;
            }

        } else if (node.parent.parent == null) {
            //if the parent is the root we make sure it is black

            if (node.parent.isRed) {
                node.parent.isRed = false;
            }

        } else if (node.parent.isRed && ((node.parent.parent.left == null ^ node.parent.parent.right == null) || (node.parent.parent.left.isRed ^ node.parent.parent.right.isRed))) {
            //if the parent is red and the aunt is null or black

            if (node.parent.isRed) {
                if (node.isLeft && node.parent.isLeft) {
                    //right rotation of the grandparent node

                    rotateRight(node.parent.parent);

                } else if (!node.isLeft && !node.parent.isLeft) {
                    //left rotation of the grandparent node

                    rotateLeft(node.parent.parent);

                } else if (node.isLeft) {
                    //right rotation of the parent node and left rotation of the grandparent node

                    rotateRight(node.parent);
                    rotateLeft(node.parent);

                } else {
                    //left rotation of the parent node and right rotation of the grandparent node

                    rotateLeft(node.parent);
                    rotateRight(node.parent);
                }
            }
        } else if (node.parent.parent.left.isRed == node.parent.parent.right.isRed) {
            //if aunt is red

            if (node.parent.isRed) {
                colorFlip(node);
            }
        }

        //make sure the root remains black
        if (root.isRed) {
            root.isRed = false;
        }

    }

    //parameters:   the tree root
    //return:   sets to default the values of all the fields
    private void deleteRBT() {
        root = null;
    }


    //sub functions

    //parameters:   the node which causes a violation
    //return:   makes the parent and the aunt black and the grandparent red
    private void colorFlip(Node node) {
        node.parent.parent.left.isRed = false;
        node.parent.parent.right.isRed = false;
        node.parent.parent.isRed = true;
        checkRBT(node.parent.parent);
    }

    //parameters:   the node which is to be rotated
    //return:   rotates the tree pivoting on the chosen node
    private void rotateLeft(Node node) {
        Node a = node;
        Node b = node.right;
        Node c = (node.right != null) ? node.right.left : null;

        //do a color flip
        boolean temp = a.isRed;
        a.isRed = b.isRed;
        b.isRed = temp;

        if (a.parent != null) {
            //first step
            if (a.isLeft) {
                a.parent.left = b;
                b.isLeft = true;
            } else {
                a.parent.right = b;
                b.isLeft = false;
            }

            //second step
            b.parent = a.parent;
        } else {
            //if we are rotated with the root as a pivot, then we change the root
            root = b;
            root.parent = null;
            root.isRed = false;
            root.isLeft = false;
        }

        //third step
        if (c != null) {
            c.parent = a;
            c.isLeft = false;
        }

        //fourth step
        b.left = a;

        //fifth step
        a.right = c;

        //sixth step
        a.parent = b;
        a.isLeft = true;
    }

    //parameters:   the node which is to be rotated
    //return:   rotates the tree pivoting on the chosen node
    private void rotateRight(Node node) {
        Node a = node;
        Node b = node.left;
        Node c = (node.left != null) ? node.left.right : null;

        //do a color flip
        boolean temp = a.isRed;
        a.isRed = b.isRed;
        b.isRed = temp;

        if (a.parent != null) {
            //first step
            if (a.isLeft) {
                a.parent.left = b;
                b.isLeft = true;
            } else {
                a.parent.right = b;
                b.isLeft = false;
            }

            //second step
            b.parent = a.parent;
        } else {
            //if we are rotated with the root as a pivot, then we change the root
            root = b;
            root.parent = null;
            root.isRed = false;
            root.isLeft = false;
        }

        //third step
        if (c != null) {
            c.parent = a;
            c.isLeft = true;
        }

        //fourth step
        b.right = a;

        //fifth step
        a.left = c;

        //sixth step
        a.parent = b;
        a.isLeft = false;
    }

    //parameters:   user input and the root of the tree
    //return:   inserts the node in the tree and then calls another function to check and correct the tree if necessary
    private void case1() {

        //take user input
        System.out.print("Insert a key: ");
        Node newNode = new Node();
        newNode.key = scan.nextInt();

        //insert the new node in the tree
        insertNode(root, newNode);

        //sout info about the newly inserted node
        if (newNode.parent == null) {
            System.out.println("Node.key = " + newNode.key + "; Node.parent = null;");
        } else {
            System.out.println("Node.key = " + newNode.key + "; Node.parent = " + newNode.parent.key + "; Node.isLeft = " + newNode.isLeft);
        }

        //check the tree for violations
        checkRBT(newNode);

    }

    //parameters:   user input
    //return:   sout to the screen the answer if the node was removed or not
    private void case2() {
        //take user input
        System.out.print("Insert the key of the node you want to remove: ");
        int key = scan.nextInt();
        Node soughtNode = searchNode(root, key);

        //check if there is such a node
        if (soughtNode == null) {
            System.out.println("No node with key of " + key + " was found.");
        } else {
            removeNode(soughtNode);
        }
    }

    //parameters:   user input
    //return:   sout to the screen the answer if the node was found or not
    private void case3() {
        //take user input
        System.out.print("Insert the key of the node you want to find: ");
        int key = scan.nextInt();
        Node soughtNode = searchNode(root, key);

        //check if there is such a node
        if (soughtNode == null) {
            System.out.println("No node with key of " + key + " was found.");
        } else {
            //check if the node is the root or a branch/leaf
            if (soughtNode.parent == null) {
                //in case the node is the root
                System.out.println("Found node with key: " + soughtNode.key + " and no parent node");
            } else {
                //in case the node is a branch/leaf
                System.out.println("Found node with key: " + soughtNode.key + " and parent: " + soughtNode.parent.key + ". Node.isLeft = " + soughtNode.isLeft);
            }
        }
    }

    //parameters:   the node which is going to disappear and the node which is going to replace the old one
    //return:   replaces the first node with the second one
    private void replaceNodes(Node toBeRemoved, Node replacement) {

        //change the child pointer in the parent node
        if (toBeRemoved.parent == null) {
            root = replacement;
        } else if (toBeRemoved.isLeft) {
            toBeRemoved.parent.left = replacement;
        } else {
            toBeRemoved.parent.right = replacement;
        }

        //reassign the children of the replacement to other nodes

        //change the children pointer of the replacement node towards the children of the node which is being removed

    }

    //parameters:
    //return:
    private void proceedToCases(Node x, Node w) {
    }

    //parameters:
    //return:
    private void removeCase1(Node x, Node w) {
    }

    //parameters:
    //return:
    private void removeCase2(Node x, Node w) {
    }

    //parameters:
    //return:
    private void removeCase3(Node x, Node w) {
    }

    //parameters:
    //return:
    private void removeCase4(Node x, Node w) {
    }
}